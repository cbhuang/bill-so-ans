============================================
Analysis of Bill's Stack Overflow Answers
============================================

Note: There is also an `R Shiny version <https://gitlab.com/cbhuang/bill-so-ans-shiny>`_
with a functionally similar frontend implemented. System prerequisites are
the same.

Introduction
=====================

Stack Overflow (SO) `reputation <https://stackoverflow.com/help/whats-reputation>`_
(abbr. "*rep*" hereafter) is a well-recognized indicator of programming proficiency.
How to earn SO rep efficiently? The project studies `the author's rep gain <https://stackoverflow.com/users/3218693>`_
on Stack Overflow based on answers posted between ``2020-09-30`` and ``2020-12-05``.

Questions of particular interest:

- Is it more efficient to **give short answers**? Efficiency may mean the following:
    + Rep gain per post
    + Rep gain per character
- Is it more efficient to **answer short questions**?
- Rep-wise speaking, is it worth answering questions from low-rep users?

For a quick overview of the dataset:

- The author has earned 3,024 rep by posting 177 answers during 45 active days.
    + 3.93 ans per day, or 67.2 rep per day
- Among the answers,
    + 98 (55.4%) were accepted
    + 40 (22.6%) did not result in rep gain
    + The average rep gain is 17.1 per answer.

N.B. The above stats were obtained on ``2020-12-06``. These figures are
subject to change due to occasional later votes and user removals.


System Prerequisites
==========================

- conda (`anaconda, miniconda <https://docs.conda.io/en/latest/miniconda.html>`_ or equivalent)
- `docker <https://docs.master.dockerproject.org/get-docker/>`_
    + Enable `execution with ordinary user <https://docs.master.dockerproject.org/engine/install/linux-postinstall/>`_.
- `docker compose <https://docs.docker.com/compose/install>`_

Test Procedure
=====================

Operate on a ``bash``-like command line interface.


1. Download and restore environment
----------------------------------------------------
.. code-block:: bash

    $ git clone bill-so-ans
    $ cd bill-so-ans
    $ conda env create --name bill-so-ans --file conda-env.yml
    $ conda activate bill-so-ans


2A. Build and run docker image locally
----------------------------------------------------

To be used when CD/CI tools were not set up.

.. code-block:: bash

    # build docker image
    (bill-so-ans) $ docker build -t bill-so-ans .

    # create container and run
    (bill-so-ans) $ docker-compose up

And then open `<http://localhost:8050>`_ in the browser.

2B. Direct execution
----------------------------

Not recommended for production use.

.. code-block:: bash

    (bill-so-ans) $ python3 app.py

    # open http://localhost:8050 in the browser
