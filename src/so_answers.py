"""
Data preprocessing
"""

import os
from pathlib import Path
import sys
from datetime import datetime, timedelta
from dateutil.tz import gettz
import json
import re
from pprint import pprint
import pandas as pd
from stackapi import StackAPI
from bs4 import BeautifulSoup
import lxml.html
from scipy.stats import linregress  #, normaltest, mannwhitneyu


# from lib.base_data_container import BaseDataContainer

# locate project base directory
if "__file__" in globals():  # console
    base_dir = Path(__file__).absolute().parents[1]
else:  # interactive
    base_dir = Path(os.getcwd())
# add to path
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))


class SoAnswers:
    """Data processing for SO data """
    
    #: str: date format
    YMD = "%Y-%m-%d"
    
    def __init__(self, conf_file=None, **kwargs):

        super().__init__(**kwargs)

        # locate configuration file
        if conf_file is None:
            self.conf_file = base_dir / "data" / "in" / "conf.json"
        else:
            self.conf_file = Path(conf_file)

        # load config file
        with open(self.conf_file) as f:
            dic = json.load(f)

        # ===================================
        # paths
        # ===================================
        #: Path: project base directory
        self.base_dir = base_dir
        #: Path: data directory
        self.data_dir = self.base_dir / "data"
        #: Path: input/download and config directory
        self.in_dir = self.data_dir / "in"
        #: Path: output data directory
        self.out_dir = self.data_dir / "out"
        #: Path: raw reputation history data
        self.raw_rep_history_file = self.in_dir / "rep_history.json"
        #: Path: raw answers data
        self.raw_ans_file = self.in_dir / "answers.json"
        #: Path: raw questions data
        self.raw_question_file = self.in_dir / "questions.json"

        # ===================================
        # SO Site Settings
        # ===================================

        # API settings
        #: StackAPI: API instance
        self.SITE = StackAPI('stackoverflow')
        #: int: SO page_size param
        self.SITE.page_size = dic["so_page_size"]
        #: int: SO max_pages param
        self.SITE.max_pages = dic["so_max_pages"]

        # SO user settings
        #: int: Bill's user ID
        self.uid = dic["uid"]
        #: str: time zone of the user
        self.time_zone = dic["time_zone"]
        #: tzfile: time zone object for internal use
        self.tz_obj = gettz(self.time_zone)
        #: str: min date of answer retrieved (YYYY-MM-DD)
        self.min_date_str = dic["min_date"]
        #: datetime: min date of answer retrieved, time zone aware
        self.min_date = self.dt_from_str_with_tz(self.min_date_str)
        #: str: max date of answer retrieved (YYYY-MM-DD)
        self.max_date_str = dic["max_date"]
        #: datetime: max date of answer retrieved, time zone aware
        self.max_date = self.dt_from_str_with_tz(self.max_date_str)

        #: int: ``begin_date`` timestamp for API use
        self.fromdate = self.get_stackapi_ts(self.min_date)
        #: int: ``end_date`` timestamp for API use
        self.todate = self.get_stackapi_ts(self.max_date, add_one_day=True)

        #: str: html parser for getting text contents
        self.html_parser = dic["html_parser"]

        # ===================================
        # UI inputs
        # ===================================

        #: datetime: begin date (from UI)
        self.begin_date = self.min_date  # apply initially, time zone aware
        #: str: str of begin_date (r-compatibility)
        self.begin_date_str = self.begin_date.strftime(self.YMD)
        #: datetime: end date (from UI)
        self.end_date = self.max_date  # apply initially, time zone aware
        #: str: str of end_date (r-compatibility)
        self.end_date_str = self.end_date.strftime(self.YMD)

        # ===================================
        # Analytical Logic
        # ===================================
        #: float: significance level
        self.sig = dic["significance_level"]

        # ===================================
        # Main Datasets
        # ===================================
        #: list: raw reputation history
        self.ls_rep = None
        #: list: raw answers
        self.ls_a = None
        #: list: raw questions to answers
        self.ls_q = None

        # (preprocessed, filtered by date range)
        #: DataFrame: reputation history
        self.df_rep = None
        #: DataFrame: answers
        self.df_a = None
        #: DataFrame: questions to answers
        self.df_q = None
        #: DataFrame: main Q&A dataset (all data)
        self.df_qa = None
        #: DataFrame: main working dataset
        self.df = None

        # ===================================
        # Results
        # ===================================

        # activity summary
        #: datetime: first answer date
        self.first_ans_date = None
        #: datetime: last answer date
        self.last_ans_date = None
        #: int: # of active dates
        self.n_active_days = None
        #: int: #answers
        self.n_ans = None

        # Reputation Distribution
        #: int: total reputation gain
        self.rep_total = None
        #: int: # zero-gain answers
        self.n_a0 = None
        #: int: # accepted answers
        self.n_acc = None
        #: DataFrame: # reputation gain
        self.df_rep_gain_count = None

        # Reputaion (total & per char) vs alen/qlen
        #: dict: rep vs answer length - result from linregress
        self.dic_rep_vs_alen = None
        #: dict: rep per character vs answer length - result from linregress
        self.dic_reppc_vs_alen = None
        #: dict: rep vs question length - result from linregress
        self.dic_rep_vs_qlen = None
        #: dict: rep per character vs question length - result from linregress
        self.dic_reppc_vs_qlen = None

        # Reputation (total) vs asker_rep
        #: dict: rep vs asker_rep
        self.dic_rep_vs_asker_rep = None

        # q-a length correlation
        #: dict: alen vs qlen
        self.dic_alen_vs_qlen = None
        # ===================================
        # Debug Settings
        # ===================================

        #: bool: debug or production mode
        self.dbg = dic["debug_mode"]

    def master_workflow(self):
        # data retrieval
        self.s01_get_reputation_history()
        self.s02_get_answers()
        self.s03_get_questions_to_answers()
        # data preprocessing
        self.s11_reputation_history()
        self.s12_answers()
        self.s13_questions_to_answers()
        self.s15_main_dataset()
        # analysis based on date-filtered results
        self.s20_filtered_analysis()

    # ===================================
    # Workflow: Data Retrieval
    # ===================================

    def s01_get_reputation_history(self):
        """Download reputation history into a json file from SO.
        Won't re-scrape if data exists.
        """
        # fresh scrape
        if not self.raw_rep_history_file.exists():
            if self.dbg:
                print("\nFetching reputation data...")

            # download
            ret = self.SITE.fetch('users/{ids}/reputation', ids=[self.uid],
                                  fromdate=self.fromdate, todate=self.todate)
            assert not ret["has_more"]   # make sure all data is scraped

            # save retrieved data
            self.ls_rep = ret["items"]  # get rid of API metadata
            with open(self.raw_rep_history_file, "w") as f:
                json.dump(self.ls_rep, f)
        # load existing
        else:
            if self.dbg:
                print("\nLoading saved reputation data...")
            with open(self.raw_rep_history_file) as f:
                self.ls_rep = json.load(f)

        if self.dbg:
            print(f"\nls_rep: {len(self.ls_rep)} items retrieved!")
            print("First 3 records:")
            pprint(self.ls_rep[:3])

    def s02_get_answers(self):
        """Download answers into a json file from SO."""

        if not self.raw_ans_file.exists():
            if self.dbg:
                print("\nFetching answer data...")

            # download
            ret = self.SITE.fetch("users/{ids}/answers", ids=[self.uid],
                                  fromdate=self.fromdate, todate=self.todate, filter="withbody")
            assert not ret["has_more"]

            # save
            self.ls_a = ret["items"]
            with open(self.raw_ans_file, "w") as f:
                json.dump(self.ls_a, f)

        else:
            if self.dbg:
                print("\nLoading saved answer data...")
            with open(self.raw_ans_file) as f:
                self.ls_a = json.load(f)

        if self.dbg:
            print(f"ls_a: {len(self.ls_a)} items retrieved!")
            print("First record:")
            pprint(self.ls_a[0])

    def s03_get_questions_to_answers(self):
        """Download questions to the answers into a json file from SO."""

        if not self.raw_question_file.exists():

            # There can be multiple answers to the same question.
            # Otherwise there would be a mismatch between input and returned lengths.
            qid_unique = list(set(dic["question_id"] for dic in self.ls_a))

            if self.dbg:
                print("\nFetching question data...")

            # download 100 records per request (Stack Exahange API limitation)
            ls_ret = [
                self.SITE.fetch("questions/{ids}", ids=qid_unique[i * 100:(i + 1) * 100], filter="withbody")
                for i in range(1 + (len(qid_unique) - 1) // 100)
            ]
            assert not ls_ret[-1]["has_more"]

            # flatten
            self.ls_q = [dic for ret in ls_ret for dic in ret["items"]]

            # check: input length == retrieved length
            assert len(self.ls_q) == len(qid_unique)

            # save
            with open(self.raw_question_file, "w") as f:
                json.dump(self.ls_q, f)

        else:
            if self.dbg:
                print("\nLoading saved question data...")

            with open(self.raw_question_file) as f:
                self.ls_q = json.load(f)

        if self.dbg:
            print(f"ls_q: {len(self.ls_q)} items retrieved!")
            print("First record:")
            pprint(self.ls_q[0])

    # ===================================
    # Workflow: Data Preprocessing
    # ===================================

    def s11_reputation_history(self):
        """Produce reputation history DataFrame."""
        # convert list to df
        self.df_rep = pd.DataFrame(self.ls_rep).drop(columns="user_id")  # single user (me)
        # timestamp to answerer datetime
        self.df_rep["on_date"] = self.to_answerer_time(self.df_rep["on_date"])

        self.df_rep.sort_values("on_date", ascending=False, inplace=True)

        if self.dbg:
            print("\nself.df_rep head and tail:")
            self.df_rep.head(3)
            self.df_rep.tail(3)

    def s12_answers(self):
        """Produce answers DataFrame."""

        self.df_a = pd.DataFrame(self.ls_a).drop(columns=["owner"])  # single owner (me)
        self.df_a["adate"] = self.to_answerer_time(self.df_a["creation_date"])

        # answer length
        self.df_a["alen"] = self.df_a["body"].map(lambda x: self.get_html_length(x, parser=self.html_parser))

        # rep gain
        self.df_a = self.compute_rep_gain(self.df_a)

        # keep only columns needed
        self.df_a = self.df_a[['is_accepted', 'score', 'adate', 'answer_id',
                               'question_id', 'rep_gain', 'alen', "rep_gain_per_alen"]] \
            .sort_values("adate", ascending=False)

        if self.dbg:
            print("\nself.df_a head and tail:")
            print(self.df_a.head(3))
            print(self.df_a.tail(3))

    def compute_rep_gain(self, df_a):
        """Compute reputation earned by this answer.

        Args:
            df_a (DataFrame)

        Returns:
            df_a

        Note:
            When Downvotes are rare, the following approximation applies. Downvotes are
            counted as -10 rep instead of -2 with this formula.

            .. code-block:: python

                df_a["rep"] = df_a["is_accepted"] * 15 + df_a["score"] * 10

        """

        # cumulative rep change
        sr_rep = self.df_rep[self.df_rep["post_type"] == "answer"]\
            .groupby("post_id")["reputation_change"].sum()

        df_a["rep_gain"] = df_a["answer_id"].map(sr_rep)\
            .fillna(0)\
            .astype(int)

        # also compute per character gain
        df_a["rep_gain_per_alen"] = df_a["rep_gain"] / df_a["alen"]

        return df_a

    def s13_questions_to_answers(self):
        """Produce questions DataFrame."""

        self.df_q = pd.DataFrame(self.ls_q)  # single owner (me)

        # ask date
        self.df_q["qdate"] = self.to_answerer_time(self.df_q["creation_date"])

        # asker reputation (at the time of data retrieval)
        self.df_q["asker_rep"] = [dic["reputation"] if "reputation" in dic else 1 for dic in self.df_q["owner"]]

        # question length
        self.df_q["qlen"] = self.df_q["body"].map(lambda x: self.get_html_length(x, parser=self.html_parser))

        # keep columns needed
        self.df_q = self.df_q[['question_id', 'qdate', 'asker_rep', 'qlen', 'tags']] \
            .sort_values("qdate", ascending=False)

        if self.dbg:
            print("\nself.df_q head and tail:")
            print(self.df_q.head(3))
            print(self.df_q.tail(3))

    def s15_main_dataset(self):
        """Main dataset ``df_qa`` as a basis of filtered analysis."""

        self.df_qa = self.df_a.merge(self.df_q, how="left", on="question_id")[[
            'answer_id', 'adate', 'rep_gain', "rep_gain_per_alen", 'is_accepted', 'score', 'alen',
            'question_id', 'qdate', 'asker_rep', 'qlen', 'tags']] \
            .sort_values("adate", ascending=False)

        if self.dbg:
            print("\nself.df head and tail:")
            print(self.df_qa.head(3))
            print(self.df_qa.tail(3))

    # ===================================
    # Workflow: Filtered Analysis
    # ===================================

    def filtered_analysis(self, begin_date=None, end_date=None):
        """Master function for analysis tasks depending on date range filter.

        Args:
            begin_date (datetime|str): beginning of answers collected (answerer's local time)
            end_date (datetime|str): end of answers collected (answerer's local time)
        """

        # default values
        if begin_date is None:
            begin_date = self.min_date
        if end_date is None:
            end_date = self.end_date

        # also accept YYYY-MM-DD strings (R-compatible)
        if isinstance(begin_date, str):
            begin_date = datetime.strptime(begin_date, self.YMD)
        if isinstance(end_date, str):
            end_date = datetime.strptime(end_date, self.YMD)

        # enforce timezone and record for global use
        self.begin_date = begin_date.replace(tzinfo=self.tz_obj)
        self.end_date = end_date.replace(tzinfo=self.tz_obj)
        # R-compatible
        self.begin_date_str = self.begin_date.strftime(self.YMD)
        self.end_date_str = self.end_date.strftime(self.YMD)

        # topics of analysis
        # data overview
        self._s21_filter_date()
        self._s22_activity_summary()
        self._s23_rep_dist()
        # main topics
        self._s31_rep_vs_alen()
        self._s32_reppc_vs_alen()
        self._s33_rep_vs_qlen()
        self._s34_reppc_vs_qlen()
        self._s35_rep_vs_asker_rep()
        # supplementary plots
        self._s41_alen_vs_qlen()

    # internal step number
    s20_filtered_analysis = filtered_analysis

    def _s21_filter_date(self):
        """Filter data by date from UI input. ``None`` means no filter.
        Produces ``self.df``, the main working dataset.
        """
        # main working dataset
        self.df = self.df_qa[(self.df_qa["adate"] >= self.begin_date) &
                             (self.df_qa["adate"] < self.end_date + timedelta(days=1))]

    def _s22_activity_summary(self):
        """Summary of activity span."""
        self.first_ans_date = self.df['adate'].min()
        self.last_ans_date = self.df['adate'].max()
        self.n_active_days = len(self.df["adate"].dt.date.drop_duplicates())
        self.n_ans = len(self.df)

        if self.dbg:
            print(f"\n- First answer : {self.first_ans_date}")
            print(f"- Latest answer: {self.last_ans_date}")
            print(f"- Active days: {self.n_active_days}")
            print(f"- Answers published: {self.n_ans} ({self.n_ans / self.n_active_days:.2f} per day)")

    def _s23_rep_dist(self):
        """Distribution of reputation"""
        self.rep_total = self.df["rep_gain"].sum()
        self.n_a0 = (self.df["rep_gain"] == 0).sum()
        self.n_acc = self.df["is_accepted"].sum()
        self.df_rep_gain_count = self.df.groupby("rep_gain").size().rename("count").reset_index()

        if self.dbg:
            print(f"\n- Total rep gain: {self.rep_total}")
            print(f"- Avg. rep gain per active day: {self.rep_total / self.n_active_days:.1f}")
            print(f"- Avg. rep gain per answer: {self.rep_total / self.n_ans:.1f}")
            print(f"- Zero-gain answers: {self.n_a0}/{self.n_ans} ({self.n_a0 / self.n_ans * 100:.1f}%)")
            print(f"- Accepted answers: {self.n_acc}/{self.n_ans} ({self.n_acc / self.n_ans * 100:.1f}%)")

    def _s31_rep_vs_alen(self):
        """Reputation vs Answer Length"""

        slope, intercept, r_value, p_value, std_err = linregress(self.df["alen"], self.df["rep_gain"])
        self.dic_rep_vs_alen = self.linregress_output_to_dict(slope, intercept, r_value, p_value, std_err)

        if self.dbg:
            print(f"\n===== Reputation vs Answer Length =====")
            print(f"Equation: y = {intercept:.3e} {slope:+.3e} * qlen")
            print(f"Correlation={r_value:.3f} (p-value={p_value:.3f})")
            print(f"Standard Error={std_err:.3e})")

    def _s32_reppc_vs_alen(self):
        """Reputation Gain per Ans Character vs Answer Length"""

        slope, intercept, r_value, p_value, std_err = linregress(
            self.df["alen"], self.df["rep_gain_per_alen"]
        )
        self.dic_reppc_vs_alen = self.linregress_output_to_dict(slope, intercept, r_value, p_value, std_err)

        if self.dbg:
            print(f"\n===== Reputation per Char vs Answer Length =====")
            print(f"Equation: y = {intercept:.3e} {slope:+.3e} * qlen")
            print(f"Correlation={r_value:.3f} (p-value={p_value:.3f})")
            print(f"Standard Error={std_err:.3e})")

    def _s33_rep_vs_qlen(self):
        """Reputation vs Question Length"""

        slope, intercept, r_value, p_value, std_err = linregress(self.df["qlen"], self.df["rep_gain"])
        self.dic_rep_vs_qlen = self.linregress_output_to_dict(slope, intercept, r_value, p_value, std_err)

        if self.dbg:
            print(f"\n===== Reputation vs Question Length =====")
            print(f"Equation: y = {intercept:.3e} {slope:+.3e} * qlen")
            print(f"Correlation={r_value:.3f} (p-value={p_value:.3f})")
            print(f"Standard Error={std_err:.3e})")

    def _s34_reppc_vs_qlen(self):
        """Reputation Gain per Ans Character vs Answer Length"""
        # NOTE: still per alen! (per qlen doesn't make sense)

        slope, intercept, r_value, p_value, std_err = linregress(
            self.df["qlen"], self.df["rep_gain_per_alen"]
        )
        self.dic_reppc_vs_qlen = self.linregress_output_to_dict(slope, intercept, r_value, p_value, std_err)

        if self.dbg:
            print(f"\n===== Reputation per Char vs Question Length =====")
            print(f"Equation: y = {intercept:.3e} {slope:+.3e} * qlen")
            print(f"Correlation={r_value:.3f} (p-value={p_value:.3f})")
            print(f"Standard Error={std_err:.3e})")

    def _s35_rep_vs_asker_rep(self):
        """Reputation gain vs asker rep. Rep per char is ignored."""

        slope, intercept, r_value, p_value, std_err = linregress(self.df["asker_rep"], self.df["rep_gain"])
        self.dic_rep_vs_asker_rep = self.linregress_output_to_dict(slope, intercept, r_value, p_value, std_err)

        if self.dbg:
            print(f"\n===== Reputation vs Asker Rep =====")
            print(f"Equation: y = {intercept:.3e} {slope:+.3e} * asker_rep")
            print(f"Correlation={r_value:.3f} (p-value={p_value:.3f})")
            print(f"Standard Error={std_err:.3e})")

    def _s41_alen_vs_qlen(self):
        """Compute corr. between alen and qlen."""

        slope, intercept, r_value, p_value, std_err = linregress(
            self.df["qlen"], self.df["alen"]
        )
        self.dic_alen_vs_qlen = self.linregress_output_to_dict(slope, intercept, r_value, p_value, std_err)

        if self.dbg:
            print(f"\n===== Answer Length vs Question Length =====")
            print(f"Equation: alen = {intercept:.3e} {slope:+.3e} * qlen")
            print(f"Correlation={r_value:.3f} (p-value={p_value:.3f})")
            print(f"Standard Error={std_err:.3e})")

    # =============================================
    # Utility functions
    # =============================================

    def get_stackapi_ts(self, dt, add_one_day=False):
        """Datetime to integer timestamp for StackAPI. Time zone aware.

        Args:
            dt (datetime): parsed datetime object
            add_one_day (bool): add one day or not

        Returns:
            int: timestamp
        """
        if add_one_day:
            dt += timedelta(days=1)
        # localize
        dt2 = dt.replace(tzinfo=self.tz_obj)
        return int(dt2.timestamp())

    def dt_from_str_with_tz(self, s):
        """``datetime`` object from ``str``, with time zone info added.

        Args:
            s (str): YYYY-MM-DD

        Returns:
            datetime: with time zone info recorded
        """
        return datetime.strptime(s, self.YMD).replace(tzinfo=self.tz_obj)

    @staticmethod
    def get_html_length(s, parser="lxml"):
        """Preprocess html text and return a reasonable length estimate.

        Args:
            s (str): raw string including tags
            parser (str): "bs4" or "lxml"
        Returns:
            int: number of characters of the preprocessed text
        """

        # tags stripped, entities encoded
        if parser == "lxml":
            s1 = lxml.html.document_fromstring(s).text_content()
        elif parser[:2] == "bs":
            s1 = BeautifulSoup(s, parser="html.parser").get_text()

        # spaces contracted
        s2 = re.sub(r"\s{2,}", " ", s1)

        return len(s2)

    def to_answerer_time(self, sr):
        """Convert UTC timestamp back to answerer local time.

        Args:
            sr (pandas.Series[str]): Series of raw UTC timestamp strings

        Returns:
            pandas.Series[datetime64[ns]]: Series of Dutch local time
        """
        return pd.to_datetime(sr, unit="s") \
            .dt.tz_localize("UTC") \
            .dt.tz_convert(self.tz_obj)

    @staticmethod
    def linregress_output_to_dict(slope, intercept, r_value, p_value, std_err):
        """Pack the parameters into dictionary for UI use."""
        return {
            "slope": slope,
            "intercept": intercept,
            "r_value": r_value,
            "p_value": p_value,
            "std_err": std_err
        }

if __name__ == '__main__':
    o = SoAnswers()
    o.master_workflow()
