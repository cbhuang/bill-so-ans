"""
Dash application for Bill's SO reputation change
"""

import sys
import os
from pathlib import Path
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import plotly.express as px
import plotly.graph_objects as go
from datetime import datetime

# locate project base directory
if "__file__" in globals():  # console
    base_dir = Path(__file__).absolute().parents[1]
else:  # interactive
    base_dir = Path(os.getcwd())
# add to path
if str(base_dir) not in sys.path:
    sys.path.append(str(base_dir))

from src.so_answers import SoAnswers

# =========================================================
# Parameters
# =========================================================
external_stylesheets = [
    dbc.themes.FLATLY,
    "/assets/custom.css"
]

# server
PORT = 8050
HOST = "0.0.0.0"

# =========================================================
# Data Preprocessing
# =========================================================
o = SoAnswers()
# TODO:
#     maybe I should retrieve all data & apply observation range on raw data
#     otherwise the analysis will be infering from future reputation gain...
o.master_workflow()

# =========================================================
# App Body & Main Layout
# =========================================================

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
# https://community.plotly.com/t/error-with-gunicorn/8247/12
server = app.server

# main layout
app.layout = html.Div(style={"padding": 10}, children=[
    dcc.Location(id='url', refresh=False),
    # main title row
    dbc.Row(
        dbc.Col(
            html.H1("Bill's Stack Overflow Answers")
        )
    ),
    html.Hr(),
    # main row
    dbc.Row([
        # main navigation panel
        dbc.Col(width=1, sm=2, children=[
            html.Div(id="main-nav", children=[
                # Topic list
                html.H3("Contents"),
                html.Hr(),
                # (A) NavLink - not working
                #     external_link reloads the entire page and rerun all analysis.... not good!
                #     There's a critical library bug
                #     ref: https://github.com/plotly/dash-core-components/issues/925
                #     workaround: https://stackoverflow.com/questions/60454165 (???)
                # dbc.Nav([
                #     dbc.NavLink("Home", active=True, href="/"),
                #     dbc.NavLink("Activity Summary", href="/activity-summary"),
                #     dbc.NavLink("Reputation Gain", href="/rep-gain"),
                # ]),
                # (B) buttons - also not working
                #     The button callback, by design, is ALWAYS fired even if not pressed....
                #     This causes all content pages to be returned at once, throwing platform-dependent error messages
                # dbc.Button("Home", id="home-button", color="primary", className="mr-1"),
                # dbc.Button("Activity Summary", id="activity-summary-button", color="secondary", className="mr-1"),
                # dbc.Button("Reputation Gain", id="rep-gain-button", color="secondary", className="mr-1"),
                #
                # (3) Use plain link (finally working)
                # html.Ul([
                #     html.Li(dcc.Link("Home", href="/", className="nav-link")),
                #     html.Li(dcc.Link("Activity Summary", href="/activity-summary", className="nav-link")),
                #     html.Li(dcc.Link("Reputation Gain", href="/rep-gain", className="nav-link")),
                #     html.Li(dcc.Link("vs Answer Length", href="/rep-vs-alen", className="nav-link")),
                #     html.Li(dcc.Link("vs A. Len. (per Char)", href="/reppc-vs-alen", className="nav-link")),
                #     html.Li(dcc.Link("vs Question Length", href="/rep-vs-qlen", className="nav-link")),
                #     html.Li(dcc.Link("vs Q. Len. (per Char)", href="/reppc-vs-qlen", className="nav-link")),
                #     html.Li(dcc.Link("vs Asker Rep", href="/rep-vs-asker-rep", className="nav-link")),
                #     html.Li(dcc.Link("Q-A Lengths", href="/alen-vs-qlen", className="nav-link")),
                # ]),
                html.Ul([
                    html.Li(dcc.Link("Home", href="/", className="nav-link")),
                ]),
                html.H4("Main Topics"),
                html.Hr(),
                html.Ul([
                    html.Li(dcc.Link("Activity Summary", href="/activity-summary", className="nav-link")),
                    html.Li(dcc.Link("Reputation Gain", href="/rep-gain", className="nav-link")),
                    html.Li(dcc.Link("vs Answer Length", href="/rep-vs-alen", className="nav-link")),
                    html.Li(dcc.Link("vs A. Len. (per Char)", href="/reppc-vs-alen", className="nav-link")),
                    html.Li(dcc.Link("vs Question Length", href="/rep-vs-qlen", className="nav-link")),
                    html.Li(dcc.Link("vs Q. Len. (per Char)", href="/reppc-vs-qlen", className="nav-link")),
                    html.Li(dcc.Link("vs Asker Rep", href="/rep-vs-asker-rep", className="nav-link")),
                ]),
                html.H4("Supplementary Info"),
                html.Hr(),
                html.Ul([
                    html.Li(dcc.Link("Q-A Lengths", href="/alen-vs-qlen", className="nav-link")),
                ])
            ])
        ]),
        dbc.Col(width=1, sm=2, children=[
            # date range picker
            html.H3("Analysis Period:"),
            # TODO: (lib bug #1406) the last date cannot be picked: https://github.com/plotly/dash/issues/1406
            dcc.DatePickerRange(
                id="date-range-select",
                min_date_allowed=o.min_date_str,
                max_date_allowed=o.max_date_str,
                start_date=o.min_date_str,
                end_date=o.max_date_str,
                initial_visible_month=o.min_date_str
            ),
            # Show how many days were included
            html.Hr(),
            html.Div(id="range-info", children=[])
        ]),
        # main contents block
        dbc.Col(width=9, sm=8, children=html.Div(id="main-contents", children=[]))
    ]),
    # TODO: tune footer row
    # html.Div(className="footer-wrapper", children=[
    #     html.Footer("""Made by Chuan-Bin "Bill" Huang, May 2021"""),
    #     html.Div(className="footer-pusher")
    # ])
    html.Footer("""Made by Chuan-Bin "Bill" Huang, May 2021""")
])

# =========================================================
# Global/State Variables
# =========================================================
transparent_layout = go.Layout(paper_bgcolor='rgba(0,0,0,0)',
                               plot_bgcolor='rgba(0,0,0,0)')


# =========================================================
# Utility Functions
# =========================================================
def txt_direction(slope):
    """Return description text for trend direction

    Args:
        slope (int|float): regression coefficient

    Returns:
        str
    """
    if slope < 0:
        return "Negative"
    elif slope > 0:
        return "Positive"
    elif slope == 0:
        return "Irrelevant"
    else:
        return "(Bad slope value)"


def txt_significance(p, lv=0.05):
    """Return significance text for a p-value

    Args:
        p (float): p-value
        lv (float): significance level

    Returns:
        str
    """

    if abs(p) < lv:
        return f"Yes (significance level = {lv})"
    else:
        return f"No (significance level = {lv})"


def txt_rep_per_1k_char(slope, unit="characters"):
    """Return reputation gain or loss when answer ro question length
    increases by 1k unit.

    Args:
        slope (float): regression coeff. of rep gain
        unit (str): name of unit

    Returns:
        str
    """
    return f"{slope * 1000.0:+.4f} per 1k {unit}"


def div_quick_insight(dic, unit="characters"):
    """General insight on regression results

    Args:
        dic (dict): output collected from ``scipy.stats.linregress``
        unit (str): unit name for effect size
    Returns:
        Div
    """
    sig = txt_significance(dic["p_value"], lv=o.sig)
    dir_ = txt_direction(dic["slope"])
    rep = txt_rep_per_1k_char(dic["slope"], unit=unit)

    return html.Div(id="quick-insight", children=[
        html.H3(f"Quick Insight"),
        html.Ul([
            html.Li(html.H4(f"Significance: {sig}")),
            html.Li(html.H4(f"Correlation: {dir_}")),
            html.Li(html.H4(f"Effect Size: {rep}")),
        ])
    ])


# =========================================================
# Tab: Homepage
# =========================================================

def div_home():
    return html.Div([
        html.H2("Introduction"),
        html.P("Stack Overflow (SO) reputation (abbr. \"rep\" hereafter) is a well-recognized indicator of programming proficiency. How to earn rep efficiently? The project studies the author's rep gain based on answers posted between 2020-09-30 and 2020-12-05."),
        html.P("The questions of particular interest are:"),
        html.Ul([
            html.Li("Is it more efficient to give short answers? Efficiency may mean the following:"),
            html.Ul([
                html.Li("Rep gain per post"),
                html.Li("Rep gain per character"),
            ]),
            html.Li("Is it more efficient to answer short questions?"),
            html.Li("Rep-wise speaking, is it worth answering questions from low-rep users?")
        ]),
        html.P("For a quick overview of summary stats:"),
        html.Ul([
            html.Li("The author has earned 2,999 rep by posting 177 answers during 45 active days."),
            html.Li("3.93 answers per day"),
            html.Li("66.6 rep gained per day")
        ]),
        html.P("Among the answers,"),
        html.Ul([
            html.Li("101 (57.1%) were accepted"),
            html.Li("41 (23.2%) did not result in rep gain"),
            html.Li("16.9 rep gain per answer"),
        ]),
        html.P("N.B. The above stats were collected on 2021-05-11. These figures are subject to change due to occasional later votes and user removals."),
        html.H4("Related Links:", style={"font-weight": "bold"}),
        html.Ul([
            html.Li(html.A("Bill's Stack Overflow profile", href="https://stackoverflow.com/users/3218693")),
            html.Li(html.A("Plotly Dash", href="https://dash.plotly.com/")),
            html.Li(html.A("Dash-bootstrap-components", href="https://dash-bootstrap-components.opensource.faculty.ai/docs/")),
            html.Li(html.A("Miniconda docker image", href="https://hub.docker.com/r/frolvlad/alpine-miniconda3/")),
        ])
    ])


# =========================================================
# Tab: Activity Summary
# =========================================================

def fig_activity_summary():
    return go.Figure(
        data=[go.Table(
            header={
                "values": ["Item", "Value"]
            },
            cells={
                "values": [
                    ["First Activity",
                     "Last Activity",
                     "#Active Days",
                     "#Answers"],
                    [o.first_ans_date,
                     o.last_ans_date,
                     o.n_active_days,
                     o.n_ans]
                ]
            }  # end cells
        )],  # end Table, data
        layout=transparent_layout
    )  # end Figure


def div_activity_summary():
    """Activity summary page"""
    return html.Div([
        html.H2("Summary of Answer Activity"),
        dcc.Graph(
            id="activity-summary-table",
            figure=fig_activity_summary()
        )  # end Graph
    ])


# =========================================================
# Tab: Reputation Gain Overview
# =========================================================

def fig_rep_gain():
    fig = px.bar(
        data_frame=o.df_rep_gain_count,
        x='rep_gain', y='count',
        title="Distribution of Reputation Gain"
    )
    # fig.update_layout(transparent_layout)
    return fig


def div_rep_gain():
    """Reputation gain page."""
    return html.Div([
        html.H2("Distibution of Reputation Gain"),
        dcc.Graph(id="rep-gain", figure=fig_rep_gain()),
        html.Div(id="rep-gain-details", children=[
            html.Ul([
                html.Li(f"Total rep gain: {o.rep_total}"),
                html.Li(f"Avg. rep gain per active day: {o.rep_total / o.n_active_days:.1f}"),
                html.Li(f"Avg. rep gain per answer: {o.rep_total / o.n_ans:.1f}"),
                html.Li(f"Zero-gain answers: {o.n_a0}/{o.n_ans} ({o.n_a0 / o.n_ans * 100:.1f}%)"),
                html.Li(f"Accepted answers: {o.n_acc}/{o.n_ans} ({o.n_acc / o.n_ans * 100:.1f}%)")
            ])
        ])  # Div rep-gain-details
    ])  # Div


# =========================================================
# Tab: Rep vs Answer Length
# =========================================================

def fig_rep_vs_alen():
    return px.scatter(
        o.df, title="Rep Gain vs. Answer Length",
        x="alen", y="rep_gain",
        trendline="ols"
    )


def div_rep_vs_alen():
    dic = o.dic_rep_vs_alen
    return html.Div([
        html.H2("Reputation Gain vs. Answer Length"),
        dcc.Graph(id="rep-vs-alen", figure=fig_rep_vs_alen()),
        html.Div(id="rep-vs-alen-details", children=[
            html.Ul([
                html.Li(f"Equation: rep_gain = {dic['intercept']:.3e} {dic['slope']:+.3e} * alen"),
                html.Li(f"Pearson Correlation: {dic['r_value']:.3f} (p-value={dic['p_value']:.3f})")
            ])
        ]),
        # insight block
        div_quick_insight(dic)
    ])


# =========================================================
# Tab: Rep per char vs Answer Length
# =========================================================

def fig_reppc_vs_alen():
    # NOTE: y variable is always rep_gain_per_alen!
    return px.scatter(
        o.df, title="Rep Gain per Character vs. Answer Length",
        x="alen", y="rep_gain_per_alen",
        trendline="ols"
    )


def div_reppc_vs_alen():
    dic = o.dic_reppc_vs_alen
    return html.Div([
        html.H2("Reputation Gain per Character vs. Answer Length"),
        dcc.Graph(id="reppc-vs-alen", figure=fig_reppc_vs_alen()),
        html.Div(id="reppc-vs-alen-details", children=[
            html.Ul([
                html.Li(f"Equation: y = {dic['intercept']:.3e} {dic['slope']:+.3e} * alen"),
                html.Li(f"Pearson Correlation: {dic['r_value']:.3f} (p-value={dic['p_value']:.3f})")
            ])
        ]),
        div_quick_insight(dic)
    ])


# =========================================================
# Tab: Rep vs Question Length
# =========================================================

def fig_rep_vs_qlen():
    return px.scatter(
        o.df, title="Rep Gain vs. Question Length",
        x="qlen", y="rep_gain",
        trendline="ols"
    )


def div_rep_vs_qlen():
    dic = o.dic_rep_vs_qlen
    return html.Div([
        html.H2("Reputation Gain vs. Question Length"),
        dcc.Graph(id="rep-vs-qlen", figure=fig_rep_vs_qlen()),
        html.Div(id="rep-vs-qlen-details", children=[
            html.Ul([
                html.Li(f"Equation: rep_gain = {dic['intercept']:.3e} {dic['slope']:+.3e} * qlen"),
                html.Li(f"Pearson Correlation: {dic['r_value']:.3f} (p-value={dic['p_value']:.3f})")
            ])
        ]),
        div_quick_insight(dic)
    ])


# =========================================================
# Tab: Rep per char vs Question Length
# =========================================================

def fig_reppc_vs_qlen():
    return px.scatter(
        o.df, title="Rep Gain per Character vs. Question Length",
        x="qlen", y="rep_gain_per_alen",
        trendline="ols"
    )


def div_reppc_vs_qlen():
    dic = o.dic_reppc_vs_qlen
    return html.Div([
        html.H2("Reputation Gain per Character vs. Question Length"),
        dcc.Graph(id="reppc-vs-qlen", figure=fig_reppc_vs_qlen()),
        html.Div(id="reppc-vs-qlen-details", children=[
            html.Ul([
                html.Li(f"Equation: y = {dic['intercept']:.3e} {dic['slope']:+.3e} * qlen"),
                html.Li(f"Pearson Correlation: {dic['r_value']:.3f} (p-value={dic['p_value']:.3f})")
            ])
        ]),
        div_quick_insight(dic)
    ])


# =========================================================
# Tab: Rep Gain vs Asker Rep
# =========================================================

def fig_rep_vs_asker_rep():
    return px.scatter(
        o.df, title="Rep Gain vs. Asker Rep",
        x="asker_rep", y="rep_gain",
        trendline="ols"
    )


def div_rep_vs_asker_rep():
    dic = o.dic_rep_vs_asker_rep
    return html.Div([
        html.H2("Reputation Gain vs. Asker Rep"),
        dcc.Graph(id="rep-vs-asker-rep", figure=fig_rep_vs_asker_rep()),
        html.Div(id="rep-vs-asker-rep-details", children=[
            html.Ul([
                html.Li(f"Equation: y = {dic['intercept']:.3e} {dic['slope']:+.3e} * alen"),
                html.Li(f"Pearson Correlation: {dic['r_value']:.3f} (p-value={dic['p_value']:.3f})")
            ])
        ]),
        div_quick_insight(dic, "Asker Reputation")
    ])


# =========================================================
# Supplementary Tab: alen vs qlen
# =========================================================

def fig_alen_vs_qlen():
    return px.scatter(
        o.df, title="Answer Length vs. Question Length",
        x="qlen", y="alen",
        trendline="ols"
    )


def div_alen_vs_qlen():
    dic = o.dic_alen_vs_qlen
    return html.Div([
        html.H2("Answer Length vs. Question Length"),
        dcc.Graph(id="alen-vs-qlen", figure=fig_alen_vs_qlen()),
        html.Div(id="alen-vs-qlen-details", children=[
            html.Ul([
                html.Li(f"Equation: alen = {dic['intercept']:.3e} {dic['slope']:+.3e} * qlen"),
                html.Li(f"Pearson Correlation: {dic['r_value']:.3f} (p-value={dic['p_value']:.3f})")
            ])
        ]),
        div_quick_insight(dic, unit="Question Length")
    ])


# =========================================================
# Callbacks
# =========================================================

def main_routing_function(pathname: str):
    """Routing function used for both url.pathname and DatePickerRange."""
    if pathname == "/":
        return div_home()
    elif pathname == "/activity-summary":
        return div_activity_summary()
    elif pathname == "/rep-gain":
        return div_rep_gain()
    elif pathname == "/rep-vs-alen":
        return div_rep_vs_alen()
    elif pathname == "/reppc-vs-alen":
        return div_reppc_vs_alen()
    elif pathname == "/rep-vs-qlen":
        return div_rep_vs_qlen()
    elif pathname == "/reppc-vs-qlen":
        return div_reppc_vs_qlen()
    elif pathname == "/rep-vs-asker-rep":
        return div_rep_vs_asker_rep()
    elif pathname == "/alen-vs-qlen":
        return div_alen_vs_qlen()
    else:
        return html.Div(f"Bad url: {pathname}")


@app.callback(Output("main-contents", "children"),
              [Input("url", "pathname")])
def main_routing_callback(pathname):
    """Main routing function (renders main_nav)."""
    return main_routing_function(pathname)


# DatePickerRange
@app.callback([Output("range-info", "children"),
               Output("main-contents", "children")],
              [Input("date-range-select", "start_date"),
               Input("date-range-select", "end_date"),
               Input("url", "pathname")])
def filtered_analysis_callback(start_date, end_date, pathname):
    """Re-analyze against the selected values of ``DatePickerRange``."""

    o.s20_filtered_analysis(start_date, end_date)

    return [
        [
            html.Br(),
            html.H4("Range Info:"),
            html.Br(),
            html.Ul([
                html.Li(html.H5(f"{(o.end_date - o.begin_date).days + 1} included days")),
                html.Li(html.H5(f"{o.n_active_days} active days")),
                html.Li(html.H5(f"{o.n_ans} answers post")),
                html.Li(html.H5(f"{o.rep_total} rep earned")),
            ])
        ],
        main_routing_function(pathname)
    ]


# run
if __name__ == '__main__':
    app.run_server(debug=o.dbg, port=PORT, host=HOST)
