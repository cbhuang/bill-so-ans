FROM frolvlad/alpine-miniconda3

# mirror project files
RUN mkdir -p /opt/bill-so-ans
WORKDIR /opt/bill-so-ans
COPY . .

# restore environment
RUN conda env create --name bill-so-ans -f conda-env-python.yml && \
    conda clean --all

# run
EXPOSE 8050
ENTRYPOINT ["conda", "run", "--no-capture-output", "-n", "bill-so-ans", \
            "gunicorn", "--workers=8", "app:server", "-b", "0.0.0.0:8050"]
