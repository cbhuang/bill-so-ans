.. bill-so-ans documentation master file, created by
   sphinx-quickstart on Tue May 11 04:09:55 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Analysis of Bill's Stack Overflow Answers
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   so_answers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
